﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MovingEntity {

	void Update () {
        Vector3 desiredDirection = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            desiredDirection += Vector3.left;
        if (Input.GetKey(KeyCode.D))
            desiredDirection += Vector3.right;
        if (Input.GetKey(KeyCode.W))
            desiredDirection += Vector3.forward;
        if (Input.GetKey(KeyCode.S))
            desiredDirection += Vector3.back;

        if (desiredDirection != Vector3.zero)
            MoveTowards(desiredDirection.normalized);
	}
}
