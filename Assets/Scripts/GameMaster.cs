﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {

    public int points;
    public int lifes;

    public Text pointsText;
    public Text lifeText;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        pointsText.text = ("Points: " + points);
        lifeText.text = ("Hearts: " + lifes);
    }
}
