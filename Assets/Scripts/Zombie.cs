﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MovingEntity {

    Vector3 targetPosition;
    Vector3 towardsTarget;

    float wanderRadius = 5f;

    void RecalculateTargetPosition(){
        targetPosition = transform.position + Random.insideUnitSphere * wanderRadius;
        targetPosition.y = 1;
    }

	// Use this for initialization
	void Start () {
        RecalculateTargetPosition();
	}
	
	// Update is called once per frame
	void Update () {
        towardsTarget = targetPosition - transform.position;
        MoveTowards(towardsTarget.normalized);

        if (towardsTarget.magnitude < 0.25f)
            RecalculateTargetPosition();

        Debug.DrawLine(transform.position, targetPosition, Color.yellow);

    }
}
