﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementComponent : MonoBehaviour {

    public float rotationSpeed;
    public float radius;
	// Use this for initialization
	void Start () {
        //transform.position = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
        //tranform.position = new Vector3(Mathf.Cos(Time.time), Mathf.Sin(Time.time), 0);
        transform.localScale = Vector3.one * (0.5f + Mathf.Cos(Time.time));
	}
}
